
**Problems we faced and steps we took to solve them**

1. In order to solve the issue we faced in our metricbeat data not showing up in Kibana we debated between removing metricbeat and installing it again versus simply upgrading metricbeat. We decided to upgrade as this seemed simpler of the two and fortunately it worked and we were able to see our data loaded in Kibana
# README #

Shared repository for team MAS2	Mississauga	to record deployment and incident documentation for the project.

### Team and Server Info ###

  appgrads4.nam.conygre.com	dbgrads4.nam.conygre.com
  
  
  	Jingwen(Wendy) Lu
	Jiayue(Carl) Niu
	Salwa Abdul Qayyum
	Xiaohui(Blair) Wang

  
### Sections ###

	1. Delivery of technical goals
	2. ELK  logfile analysis
	3. ITRS monitoring
	4. AppDynamics monitoring



### How do we deploy? ###

	
	1. ssh into appgrads4.nam.conygre.com and git clone Moodle application
	2. write and run docker-compose file, creating 2 containers where the mysql server talks to the host server (Moodle)
	3. now Moodle is running on 172.31.10.147!
